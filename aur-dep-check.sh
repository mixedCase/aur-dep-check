#!/bin/bash
export LANG=C

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

ldd $1 | # get shared libraries
awk '/=>/{print $(NF-1)}' | # get library paths only and skip linux.so's
LANG=C xargs pacman -Qo | # check which packages they belong to
awk '{print $(NF-1)}' | # get package names only
sort -u | # delete repeats
awk 'NR==FNR{a[$0];next} !($0 in a)' <(pacman -Sg base base-devel | cut -d\  -f2 | xargs -L 1 pactree -u) - | # remove packages pulled by base/base-devel
python3 $DIR/check-redundant-deps.py
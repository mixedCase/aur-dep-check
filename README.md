# aur-dep-check

This tool allows you to point to a dynamically-linked binary and be returned an optimized list of package dependencies ready for the AUR.

As this wasn't written with robustness in mind, it's probbaly not a good idea to set it up as part of an automatic process; but hopefully it'll help you in writing PKGBUILDs.

import sys
import subprocess

lines = [l.strip() for l in sys.stdin.readlines()]
if len(lines) == 0:
    sys.exit()


def pactree(a, pkg):
    return subprocess.Popen(
        ['pactree', '-u', pkg], stdout=subprocess.PIPE).communicate()[0].decode("utf-8").splitlines()


def optimize_list(lines, first):
    if len(lines) == 1:
        return lines
    ref_pkg = lines[0]
    if first is None:
        first = ref_pkg
    elif first == ref_pkg:
        return lines
    rest = lines[1:]
    redundant = []
    for dep in pactree("-u", ref_pkg):
        for r in rest:
            if dep.strip() == r:
                redundant.append(r)
    return optimize_list(
        list(set(rest).symmetric_difference(redundant)) + [ref_pkg],
        first
    )

print(optimize_list(lines, None))
